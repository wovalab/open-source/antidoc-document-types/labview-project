= Analog General

NOTE: This section is an extract of the following https://www.ni.com/en/support/documentation/supplemental/17/specifications-explained--c-series-modules.html[document]

== Accuracy gain and offset
Accuracy refers to how close to the correct value of a measurement is. Absolute Accuracy at Full Scale is a calculated theoretical accuracy assuming the value being measured is the maximum voltage supported in a given range. The accuracy of a measurement will change as the measurement changes, so to be able to make a comparison between modules, the accuracy at full scale is used. Note that absolute accuracy at full scale makes assumptions about environment variables, such as 25 °C operating temperature, that may be different in practice.

Nominal Range Positive Full Scale:: The ideal maximum positive value that can be measured in a particular range
Nominal Range Negative Full Scale:: The ideal maximum negative value that can be measured in a particular range
Calibrated Gain Error:: Gain error inherent to the instrumentation amplifier and is known to exist after a self-calibration
Gain Tempco:: The temperature coefficient that describes how temperature impacts the gain of the amplifier compared to the temperature at last self-calibration
Calibrated Offset Error:: Offset error inherent to the instrumentation amplifier and is known to exist after a self-calibration
INL Error (relative accuracy resolution):: The maximum deviation from the voltage output of an ADC to the ideal output. Can be thought of as worst case DNL.
Offset Tempco:: The temperature coefficient that describes how temperature affects the offset in an ADC conversion compared to the temperature at last self-calibration

== Example

.Accuracy table example
image::accuracy-offset-error.png[width=75%]

NI 9223 publishes the table above under the accuracy section. For example, when the module is configured to return calibrated data and 5 VDC is being measured, the maximum expected gain error for the full operating temperature range is ±0.20% x 5 V = ±10 mV. The offset error is ±0.10% x 10 V = ±10 mV, so the total uncertainty when measuring a 5 V signal will be 5 V ±20 mV.