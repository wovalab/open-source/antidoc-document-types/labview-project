## Summary

(Summarize the new feature concisely)

## Use cases

(Which problem or situation the new feature is going to address)

## What is the expected new behavior?

(What should be the use of the new feature)

## Possible implementation

(If you can, explain how the implementation of the new feature should be done)
